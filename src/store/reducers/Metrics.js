import * as actions from '../actions';

const thirtyMinutes = 30 * 60 * 1000;

const initialState = {
  options: [],
  watched: [],
  measurements: {},
  listeners: [],
  queryAfter: Date.now() - thirtyMinutes
};

const createTitle = camelCase => {
  const words = camelCase.replace(/([A-Z])/g, ',$1').split(',');
  return words.map(word => word[0].toUpperCase() + word.substr(1)).join(' ');
}

const measurementsReceived = (state, action) => {
  const { getMultipleMeasurements } = action;;

  const measurements = getMultipleMeasurements.reduce((results, next) => {
    const { metric, measurements } = next;
    const lastMetric = measurements[measurements.length - 1];
    return {
      ...results,
      [metric]: {
        name: createTitle(metric),
        measurements,
        ...lastMetric,
      }
    }
  }, {});

  // notify all listeners of update
  state.listeners.forEach(listener => listener.changeMetrics(measurements));

  return {
    ...state,
    measurements
  }
};

const newMeasurementReceived = (state, action) => {
  const { newMeasurement } = action;
  const { metric, ...rest } = newMeasurement;

  // notify all listeners of update
  state.listeners.forEach(listener => listener.newMeasurement(newMeasurement));
  
  return {
    ...state,
    measurements: {
      ...state.measurements,
      [metric]: {
        ...state.measurements[metric],
        ...rest,
      }
    }
  }
};

const metricsRecieved = (state, action) => {
  const { getMetrics } = action;

  const titles = getMetrics.reduce((result, next) => {
    return {
      ...result,
      [next]: createTitle(next)
    }
  }, {});

  return {
    ...state,
    options: getMetrics,
    titles 
  };
};


const watchMetrics = (state, action) => {
  const { metrics } = action;

  return {
    ...state,
    watched: metrics,
    queryAfter: Date.now() - thirtyMinutes
  }
}

const unsubscribe = (state, action) => {
  const { component } = action;
  return {
    ...state,
    listeners: state.listeners.filter(x => x !== component)
  }
}

const subscribe = (state, action) => {
  const { component } = action;
  return {
    ...state,
    listeners: [...state.listeners, component]
  }
} 

const handlers = {
  [actions.MEASUREMENT_DATA_RECIEVED]: measurementsReceived,
  [actions.NEW_MEASUREMENT_DATA_RECIEVED]: newMeasurementReceived,
  [actions.METRICS_DATA_RECIEVED]: metricsRecieved,
  [actions.WATCH_METRICS]: watchMetrics,
  [actions.SUBSCRIBE_TO_MEASUREMENTS]: subscribe,
  [actions.UNSUBSCRIBE_TO_MEASUREMENTS]: unsubscribe,
};

export default (state = initialState, action) => {
  const handler = handlers[action.type];
  if (typeof handler === 'undefined') return state;
  return handler(state, action);
};
