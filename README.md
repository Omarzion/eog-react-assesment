## Create React App Visualization

This assessment was bespoke handcrafted for adam.

Read more about this assessment [here](https://react.eogresources.com)

I apologize for the amount of time it took.

I implemented subscriptions as a finishing up step and that was when I realized that Chart.js had poor performance with my basic implementation, leading me to spend an exorbitant amount of time finding another library to suit my needs (so much in fact I might have been able to build a custom one in less time).

After preparing to submit this.... I finally read the help page and saw that you had helpfully posted links to decent graphing libraries