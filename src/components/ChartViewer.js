import React, { useEffect } from 'react';
import Chart from "./Chart";

import { useQuery, useSubscription } from 'urql';
import { useDispatch, useSelector } from 'react-redux';

import * as actions from '../store/actions';
import api from '../store/api';

const getMeasurements = state => {
  const { timeSteps, measurements, watched, queryAfter } = state.metrics;

  const chartData = {
    labels: timeSteps,
    datasets: measurements
  }

  return {
    chartData,
    metrics: watched,
    queryAfter
  }
}

const useRequestHandler = (response, type) => {
  const dispatch = useDispatch();

  useEffect(() => {
    const { error, data } = response;
    if (error) {
      dispatch({ type: actions.API_ERROR, error: error.message });
    }
    if (!data) return;

    if (type) {
      dispatch({ ...data, type });
    }
  }, [response, type, dispatch]);

  return [response.fetching]
}

const useMetrics = (metrics, after) => {
  const dispatch = useDispatch();
  const [response] = useQuery({
    query: api.getMultipleMeasurements,
    requestPolicy: 'cache-first',
    variables: {
      measurements: metrics.map(metric => ({ metricName: metric, after }))
    },
  });

  const [subResponse] = useSubscription(
    { query: api.newMeasurementSubscription },
    (_, response) => {
      const { newMeasurement } = response;
      dispatch({ type: actions.NEW_MEASUREMENT_DATA_RECIEVED, newMeasurement })
    }
  );

  useRequestHandler(response, actions.MEASUREMENT_DATA_RECIEVED);
  useRequestHandler(subResponse);
};

export default props => {
  const { chartData, metrics, queryAfter } = useSelector(getMeasurements);
  useMetrics(metrics, queryAfter);

  return <Chart data={chartData} {...props} />
};
