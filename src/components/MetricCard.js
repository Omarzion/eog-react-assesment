import React from 'react';
import { Card, CardHeader, CardContent, useTheme, CircularProgress } from "@material-ui/core";
import Typography from '@material-ui/core/Typography';
import { useSelector } from "react-redux";

const getLatestMetric = metricName => state => {
  const metric = state.metrics.measurements[metricName];
  if (!metric) return;

  return {
    title: metric.name || metricName,
    unit: metric.unit,
    value: metric.value,
  }
}

export default props => {
  const { index } = props;
  const metric = useSelector(getLatestMetric(props.metric));
  const colors = useTheme().palette.chartColors;
  if (!metric) return <CircularProgress />

  return (
    <Card>
      <CardHeader title={metric.title} />
      <CardContent>
        <Typography
          style={{ color: colors[index] }}
          component="h4"
          variant="h4"
        >
          {metric.value}{metric.unit}
        </Typography>
      </CardContent>
    </Card>
  )
}