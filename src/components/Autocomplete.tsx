import React from 'react';
import Chip from './Chip';
import { TextField, Checkbox } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

interface AutocompleteProps {
  options: string[];
  values: string[];
  onChange: (values: string[]) => any;
  labels: { [key: string]: string }
}

interface OptionType {
  value: string
}


// Deal with autocomplete bug, removing item actually duplicates it
// So we completely remove all duplicates
const removeDuplicates = (item: string, i: number, arr: string[]) => {
  return arr.indexOf(item) === arr.lastIndexOf(item)
}

export default (props: AutocompleteProps) => {
  const { options, values, onChange, labels } = props;

  const getText = (value: string) => (labels && labels[value]) || value;

  return (
    <Autocomplete
      multiple
      autoHightlight
      options={options.map(v => ({ value: v }))}
      disableCloseOnSelect
      getOptionLabel={o => o.value}
      value={values.map(v => ({ value: v }))}
      onChange={(e, metrics: OptionType[]) => {
        const results = metrics.map(m => m.value).filter(removeDuplicates);
        onChange(results);
      }}
      renderOption={option => (
        <>
          <Checkbox
            style={{ marginRight: 8 }}
            checked={values.indexOf(option.value) > -1}
          />
          {getText(option.value)}
        </>
      )}
      renderTags={(option, { className }) => option.map((option: OptionType, index: number) => (
        <Chip
          key={index}
          variant="outlined"
          tabIndex={-1}
          label={getText(option.value)}
          className={className}
          onDelete={() => onChange(values.filter(m => m !== option.value))}
        />
      ))}
      renderInput={params => {
        return (
          <TextField
            {...params}
            variant='outlined'
            placeholder='Watch Metrics'
            fullWidth
          />
        )
      }}
    />
  )
}