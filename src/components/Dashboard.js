import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ChartViewer from './ChartViewer';
import { LinearProgress } from '@material-ui/core';
import api from '../store/api';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery, Provider } from 'urql';
import * as actions from '../store/actions';
import MetricCard from './MetricCard';
import Autocomplete from './Autocomplete';
import apiClient from '../graphqlClient';

const useStyles = makeStyles({
  container: {
    display: 'grid',
    gridTemplateColumns: '4fr 1fr',
    gridGap: '1em 1em',
    padding: '1em',
    height: '100%',
    boxSizing: 'border-box',
    overflow: 'hidden',
  },
  sidebar: {
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden'
  },
  cardContainer: {
    marginTop: '1em',
    overflow: 'auto',
    display: 'grid',
    gridTemplateColumns: 'repeat(2, 1fr)',
    gridGap: '1em 1em'
  }
});

const getMetrics = state => {
  return state.metrics;
}

export default () => {
  return (
    <Provider value={apiClient}>
      <Dashboard />
    </Provider>
  )
}

const useMetrics = () => {
  const dispatch = useDispatch();
  const [{ fetching, data, error }] = useQuery({ query: api.getMetrics });
  useEffect(() => {
    if (error) {
      dispatch({ type: actions.API_ERROR, error: error.message });
      return;
    }
    if (!data) return;
    const { getMetrics } = data;
    dispatch({ type: actions.METRICS_DATA_RECIEVED, getMetrics });
  }, [data, fetching, error, dispatch]);
  return fetching;
}

const Dashboard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const metrics = useSelector(getMetrics);
  const fetching = useMetrics();

  const { watched, titles, options } = metrics;

  if (fetching) return <LinearProgress />;

  return (
    <div className={classes.container}>
      <ChartViewer />
      <div className={classes.sidebar}>
        <Autocomplete
          options={options}
          values={watched}
          onChange={metrics => dispatch({ type: actions.WATCH_METRICS, metrics })}
          labels={titles}
        />
        <div className={classes.cardContainer}>
          {watched.map((metric, i) => <MetricCard key={metric} index={i} metric={metric} />)}
        </div>
      </div>
    </div>
  )
}