import React, { Component } from 'react';
import Plotly from 'plotly.js';
import { connect } from 'react-redux';
import * as actions from '../store/actions';
import { withTheme } from '@material-ui/styles';

const layout = {
  responsive: true,
  displayModeBar: false,
  xaxis: {
    type: 'date',
    domain: [0, 1],
    automargin: true,
  },
}

const reduceMeasurements = measurements => {
  return measurements.reduce(([x, y], { at, value }) => [[...x, at], [...y, value]], [[], []])
}

class Chart extends Component {
  constructor(props) {
    super(props);
    this.axisSpacer = 0.04;
    this.traces = [];
    this.state = {};
  }

  createLayout = metrics => {
    const axi = [...new Set(Object.keys(metrics).map(key => metrics[key].unit))];
    const finishedLayout = { ...layout };
    const unitAxis = {};
    for (let i = 0; i < axi.length; i++) {
      const axisName = `yaxis${i > 0 ? i + 1 : ''}`;
      const axis = {
        [axisName]: {
          title: `${axi[i]}`,
          titlePosition: 'top',
          anchor: 'free',
          overlaying: i > 0 ? 'y' : undefined,
          side: 'left',
          position: i * this.axisSpacer,
        }
      }
      Object.assign(finishedLayout, axis)
      unitAxis[axi[i]] = axisName.replace('axis', '');
    }
    finishedLayout.xaxis.domain[0] = (axi.length - 1) * this.axisSpacer + 0.01
    return {
      layout: finishedLayout,
      unitAxis
    }
  }

  createInitialData = (metrics, unitAxis) => {
    const colors = this.props.theme.palette.chartColors;
    const keys = Object.keys(metrics);
    this.traces = keys;
    const dataset = [];
    for (let i = 0; i < keys.length; i++) {
      const metric = metrics[keys[i]];
      const { name, unit, measurements } = metric;
      const [x, y] = reduceMeasurements(measurements);
      dataset.push({
        x,
        y,
        name: `${name} (${unit})`,
        marker: {
          color: colors[i]
        },
        type: 'scatter',
        yaxis: unitAxis[unit],
        overlapping: 'y'
      });
    }
    return dataset;
  }

  changeMetrics = (metrics) => {
    if (!this.ref) return;
    const { unitAxis, layout } = this.createLayout(metrics);
    const data = this.createInitialData(metrics, unitAxis);

    Plotly.newPlot(this.ref, data, layout, { displaylogo: false });
  }

  newMeasurement = (measurement) => {
    const { metric, value, at } = measurement;
    const trace = this.traces.indexOf(metric);
    if (trace < 0) return;
    const update = { y: [[value]], x: [[at]] };
    Plotly.extendTraces(this.ref, update, [trace]);
    // Plotly.relayout(this.ref, 'xaxis.range', [Date.now() - 30*60*1000, Date.now() + 5000]);
  }

  componentDidMount() {
    this.props.dispatch({ type: actions.SUBSCRIBE_TO_MEASUREMENTS, component: this });
  }

  componentWillUnmount() {
    this.props.dispatch({ type: actions.UNSUBSCRIBE_TO_MEASUREMENTS, component: this });
    Plotly.purge(this.ref);
  }

  render() {
    return <div ref={ref => this.ref = ref} />
  }
}

export default withTheme(connect()(Chart));
