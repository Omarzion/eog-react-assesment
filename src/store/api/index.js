const getMetrics = `
query {
  getMetrics
}`

const getMultipleMeasurements = `
query($measurements: [MeasurementQuery]) {
  getMultipleMeasurements(input: $measurements) {
    metric
    measurements {
      at
      value
      unit
    }
  }
}
`;

const newMeasurementSubscription = `
subscription {
  newMeasurement {
    metric
    at
    value
    unit
  }
}`;

const getWeatherForLocation = `
query($latLong: WeatherQuery!) {
  getWeatherForLocation(latLong: $latLong) {
    description
    locationName
    temperatureinCelsius
  }
}
` 

export default {
  getMetrics,
  getMultipleMeasurements,
  newMeasurementSubscription,
  getWeatherForLocation
};
